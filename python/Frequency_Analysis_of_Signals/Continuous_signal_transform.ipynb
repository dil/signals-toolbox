{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Continuous time signal transforms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Preamble\n",
    "Start by importing the Python libraries that we will require"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sympy as sp\n",
    "import numpy as np\n",
    "import mpmath as mp\n",
    "import matplotlib.pyplot as plt\n",
    "from mpl_toolkits.mplot3d import Axes3D \n",
    "from ipywidgets import interactive, widgets\n",
    "from IPython.display import display"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And define a function that will return true if running in a Jupyter Notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def is_jupyter():\n",
    "    \"\"\"Return true if running in a Jupyter Notebook\"\"\"\n",
    "    try:\n",
    "        if get_ipython().__class__.__name__ == 'ZMQInteractiveShell':\n",
    "            return True\n",
    "        else:\n",
    "            return False\n",
    "    except: \n",
    "        return False"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### User specified parameters\n",
    "\n",
    "The following parameters can be specified. \n",
    "\n",
    "Parameter | Meaning\n",
    "--------- | -------\n",
    "<code>signal_length</code> | The length of signal in the time domain (s) (e.g. 1)\n",
    "<code>delay</code> | Set delay in the time domain (s) (e.g. 0)\n",
    "<code>plot_width</code> | The width of frequency domain plot (rad/s) (e.g. 10)\n",
    "<code>waveform</code> | Select the waveform to use (e.g. 4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "signal_length = 1\n",
    "delay = 0\n",
    "plot_width = 10\n",
    "waveform = 4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Symbols\n",
    "Define symbols to be used"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t = sp.Symbol('t')\n",
    "omega = sp.Symbol('omega')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The signal and its transform\n",
    "First we need to define the signal and its transform.  It is a finite duration, continuous signal, so will have a continuous aperiodic transform.  Later on we will want to be able to delay the signal, so add delay as a parameter that we can alter"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Waveform definition\n",
    "Note that the list of waveforms can be added to by users"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First define the set of waveforms that are available.  More can be added if desired\n",
    "\n",
    "def signal_set(delay = 0):\n",
    "    \"\"\"\n",
    "        Define the signals that may be used.  Additional signals can be added to this list\n",
    "    \"\"\"\n",
    "    # Use sympy.Piecewise function Piecewise( (expr,cond), (expr,cond), ... ) to define a continuous function\n",
    "    waveforms = {\n",
    "        # Long rectangular pulse centred on delay\n",
    "        1: sp.Piecewise(\n",
    "            # Rectangular pulse, with height 1 between the signal limits\n",
    "            (1, (t > -(signal_length*1.5)+delay) & (t < (signal_length*1.5)+delay)),\n",
    "            # and elsewhere it is zero\n",
    "            (0, True)\n",
    "        ),\n",
    "        \n",
    "        # Rectangular pulse centred on delay\n",
    "        2: sp.Piecewise(\n",
    "            # Rectangular pulse, with height 1 between the signal limits\n",
    "            (1, (t > -signal_length+delay) & (t < signal_length+delay)),\n",
    "            # and elsewhere it is zero\n",
    "            (0, True)\n",
    "        ),\n",
    "        \n",
    "        # Short rectangular pulse centred on delay\n",
    "        3: sp.Piecewise(\n",
    "            # Rectangular pulse, with height 1 between half the signal limits\n",
    "            (1, (t > -(signal_length/2)+delay) & (t < (signal_length/2)+delay)),\n",
    "            # and elsewhere it is zero\n",
    "            (0, True)\n",
    "        ),\n",
    "        \n",
    "        # Ramp function centred on delay\n",
    "        4: sp.Piecewise(\n",
    "            (t-delay, (t > -signal_length+delay) & (t < signal_length+delay)),\n",
    "            (0, True)\n",
    "        ),\n",
    "        \n",
    "        # Triangular function centred on delay\n",
    "        5: sp.Piecewise(\n",
    "            # The rising section\n",
    "            (signal_length+(t-delay), (t < delay) & (t > -signal_length+delay)),\n",
    "            # and the falling section\n",
    "            (signal_length-(t-delay), (t > delay) & (t < signal_length+delay)),\n",
    "            (0, True)\n",
    "        ),\n",
    "    }\n",
    "    \n",
    "    return waveforms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then define the functions to access the waveforms created above"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def signal(waveform = 1, delay = 0):\n",
    "    \"\"\"\n",
    "        Define the signal to be used\n",
    "    \"\"\"\n",
    "\n",
    "    waveforms = signal_set(delay)\n",
    "    \n",
    "    return waveforms.get(waveform, 0)\n",
    "\n",
    "def number_of_waveforms():\n",
    "    \"\"\"\n",
    "        Count the number of waveforms defined\n",
    "    \"\"\"\n",
    "    \n",
    "    return len(signal_set())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Select function\n",
    "The signal is generated by the function defined above, and then we create a plot of the signal.  This is done by converting the symbolic function to a set of samples that can be plotted in figures."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "time_equation = signal(waveform = waveform, delay = delay)\n",
    "\n",
    "plot_limits = [-signal_length*2, signal_length*2]\n",
    "time_function = sp.lambdify(t, time_equation, 'numpy')\n",
    "time_scale = np.linspace(plot_limits[0], plot_limits[1],400)\n",
    "time_values = time_function(time_scale)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now plot the signal waveform"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enlarge the figure, label and axis size\n",
    "plt.rcParams['figure.figsize'] = 16, 8\n",
    "plt.rcParams.update({'font.size': 16})\n",
    "\n",
    "plt.figure()\n",
    "ax = plt.gca()\n",
    "ax.plot(time_scale, time_values)\n",
    "plt.xlim(plot_limits[0], plot_limits[1])\n",
    "\n",
    "ax.set_xlabel(\"Time (s)\")\n",
    "ax.set_ylabel('Amplitude')\n",
    "ax.set_title('$x(t)$');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Compute the transform\n",
    "Now compute the transform of the signal.  We will use the complex form of the Fourier transform, not the trigonometric form.  Expressing the transform in Hertz is achieved using:\n",
    "\\begin{equation}\n",
    "    X(F) = \\int_{-\\infty}^{\\infty} x(t) e^{-j2\\pi F t} dt\n",
    "\\end{equation}\n",
    "or in radians per second it is given by:\n",
    "\\begin{equation}\n",
    "    X(\\omega) = \\int_{-\\infty}^{\\infty} x(t) e^{-j\\omega t} dt\n",
    "\\end{equation}\n",
    "\n",
    "In the python code, the transform is computed using symbolic maths"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute the transform of the signal\n",
    "transform = sp.integrate(time_equation*sp.exp(-sp.I*omega*t), (t, -sp.oo, sp.oo))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The result is then converted to a set of samples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Acquire samples of the transform\n",
    "transform_function = sp.lambdify(omega, transform, modules='numpy')\n",
    "plot_limits = [-plot_width, plot_width]\n",
    "frequency = np.linspace(-plot_width, plot_width, 400)\n",
    "transform_values = transform_function(frequency)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot the magnitude"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "ax = plt.gca()\n",
    "ax.plot(frequency, 20*np.log10(abs(transform_values)))\n",
    "left, right = plt.xlim()\n",
    "plt.xlim(left+1, right-1)\n",
    "\n",
    "ax.set_xlabel(\"Frequency (rad/s)\")\n",
    "ax.set_ylabel('Magnitude (dB)')\n",
    "ax.set_title('$X(\\omega)$')\n",
    "\n",
    "# Save figure in python or ipython system\n",
    "if not is_jupyter(): plt.savefig('odd_cont_time_magnitude.pdf')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot the phase"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "ax = plt.gca()\n",
    "ax.plot(frequency, np.angle(transform_values))\n",
    "left, right = plt.xlim()\n",
    "plt.xlim(left+1, right-1)\n",
    "\n",
    "ax.set_yticks = ([-np.pi/2, 0, np.pi/2], ['$-\\pi/2$', '0', '$\\pi/2$'])\n",
    "ax.set_xlabel('Frequency (rad/s)')\n",
    "ax.set_ylabel('Phase (rad)')\n",
    "ax.set_title('$X(\\omega)$')\n",
    "\n",
    "# Save figure in python or ipython system\n",
    "if not is_jupyter(): plt.savefig('odd_cont_time_phase.pdf')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Show Signal and its Fourier transform in 3d\n",
    "For the 3D plot we want to be able to adjust the delay to see the effect of phase changes.  In order to do this, the plotting process must be created as a function, so we define this first."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def func(waveform, delay):\n",
    "    \"\"\"\n",
    "        Function for interactive use, \n",
    "        Plot input signal with delay and its Fourier transform in 3d\n",
    "    \"\"\"\n",
    "    \n",
    "    t = sp.Symbol('t')\n",
    "    x = sp.Symbol('x')\n",
    "    omega = sp.Symbol('omega')\n",
    "    transform = sp.Symbol('transform')\n",
    "    \n",
    "    x=signal(waveform, delay)\n",
    "    \n",
    "    # Lambdify function returns an anonymous function x for \n",
    "    # calculation of numerical values with variable t.\n",
    "    x_function = sp.lambdify(t, x, 'numpy')\n",
    "\n",
    "    # Compute the transform of the example in the notes\n",
    "    transform = sp.integrate(x*sp.exp(-sp.I*omega*t), (t, -sp.oo, sp.oo))\n",
    "\n",
    "    # Acquire the samples of the transform\n",
    "    transform_function = sp.lambdify(omega, transform, 'numpy')\n",
    "    frequency = np.linspace(-plot_width, plot_width, 400)\n",
    "    transform_values = transform_function(2*np.pi*frequency)\n",
    "\n",
    "    plt.rcParams['figure.figsize'] = 14, 13\n",
    "    plt.rcParams.update({'font.size': 15})\n",
    "\n",
    "    # Plot signal\n",
    "    fig = plt.figure()\n",
    "    ax1 = plt.subplot(221)\n",
    "    t = np.arange(-20,  20, 0.001)\n",
    "    x_samples = x_function(t)\n",
    "\n",
    "    plt.xlim(-5, 5)\n",
    "    sig_limit=np.ceil(max(abs(x_samples))+0.1)\n",
    "    plt.ylim(-sig_limit, sig_limit)\n",
    "    plt.plot(t, x_samples)\n",
    "    plt.title('Input Signal')\n",
    "    plt.xlabel('Time (s)')\n",
    "\n",
    "    # Plot Fourier transform value in 3d\n",
    "    ax2 = plt.subplot(222, projection='3d')\n",
    "\n",
    "    ax2.plot(frequency, transform_values.real, transform_values.imag)\n",
    "    \n",
    "    # Format the 3D axes so that the signal fills the plot\n",
    "    trans_limit=np.sqrt(np.ceil(max(np.abs(transform_values))))\n",
    "\n",
    "    xlabel = ax2.set_xlabel('Frequency(rad/s)', labelpad=10)\n",
    "    ylabel = ax2.set_ylabel('Real', labelpad=20)\n",
    "    zlabel = ax2.set_zlabel('Imaginary', labelpad=10)\n",
    "    ax2.set_ylim(-trans_limit, trans_limit)\n",
    "    ax2.set_xlim(-plot_width, plot_width)\n",
    "    ax2.set_zlim(-trans_limit, trans_limit)\n",
    "    plt.title('Fourier Transform')\n",
    "    plt.tight_layout()\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we create the interactive display with the set of possible delays that the user can view.\n",
    "\n",
    "A change in the delay of a signal results in a phase shift in the frequency domain that is proportional to the frequency.  This is viewed as a rotation in the complex plane.\n",
    "\\begin{equation}\n",
    "\\int_{-\\infty}^{\\infty} x(t-\\tau) e^{-j\\omega t} dt = e^{-j\\omega\\tau}X(\\omega)\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y = interactive(func, waveform = widgets.IntSlider(min = 1, max = number_of_waveforms(), step = 1, value = waveform), delay = [0, 0.01, 0.02, 0.05, 0.075, 0.1, 0.2, 0.3, 0.4])\n",
    "display(y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "© The University of Edinburgh: Produced by D. Laurenson, School of Engineering. Initial code conversion by Xing Zixiao."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
